# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import datetime
from django.contrib import admin

# Create your models here.
class Stock(models.Model):
    item_name= models.CharField(max_length=200)
    brand = models.CharField(max_length=200)
    description = models.CharField(max_length=800)
    item_type = models.CharField(max_length=200)
    price = models.IntegerField(default=0)
    def __str__(self):
        return str (self.item_name)